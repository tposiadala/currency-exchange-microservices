package com.tposiadala.microservices.apigateway.config;

import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ApiGatewayConfiguration {

    @Bean
    public RouteLocator gatewayRouter(RouteLocatorBuilder builder) {
        return builder.routes()
                .route(p -> p.path("/get")
                        .filters(f -> f
                                .addRequestHeader("MyHeader", "MyURI")
                                .addRequestParameter("Param", "MyValue"))
                        .uri("http://httpbin.org:80"))
                .route(p -> p.path("/api/currency-exchange/**")
                        .uri("lb://currency-exchange"))
                .route(p -> p.path("/api/currency-conversion/**")
                        .uri("lb://currency-conversion"))
                .route(p -> p.path("/api/currency-conversion-feign/**")
                        .uri("lb://currency-conversion"))
                .route(p -> p.path("/api/currency-conversion-new/**")
                        .filters(f -> f.rewritePath(
                                "/api/currency-conversion-new/(?<segment>.*)",
                                "/api/currency-conversion-feign/${segment}"))
                        .uri("lb://currency-conversion"))
                .build();
    }
}
