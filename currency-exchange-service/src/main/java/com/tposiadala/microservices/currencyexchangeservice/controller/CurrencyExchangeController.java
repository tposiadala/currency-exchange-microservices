package com.tposiadala.microservices.currencyexchangeservice.controller;

import com.tposiadala.microservices.currencyexchangeservice.model.CurrencyExchange;
import com.tposiadala.microservices.currencyexchangeservice.service.CurrencyService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class CurrencyExchangeController {

    private final CurrencyService currencyService;

    @GetMapping("/currency-exchange/from/{from}/to/{to}")
    public CurrencyExchange retrieveExchangeValue(@PathVariable String from, @PathVariable String to) {
        return currencyService.getExchangeValue(from, to);
    }

    @GetMapping("/currency-exchange")
    public List<CurrencyExchange> getAllValues() {
        return currencyService.getAllValues();
    }
}
