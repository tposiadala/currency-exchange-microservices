package com.tposiadala.microservices.currencyexchangeservice.service;

import com.tposiadala.microservices.currencyexchangeservice.model.CurrencyExchange;
import com.tposiadala.microservices.currencyexchangeservice.repository.CurrencyExchangeRepository;
import lombok.AllArgsConstructor;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class CurrencyService {

    private final CurrencyExchangeRepository repository;
    private final Environment environment;

    public List<CurrencyExchange> getAllValues() {
        return repository.findAll();
    }

    public CurrencyExchange getExchangeValue(String from, String to) {
        CurrencyExchange currencyExchange = repository.findByFromAndTo(from, to)
                .orElseThrow(() -> new RuntimeException("Unable to find conversion from " + from + "to " + to));
        String port = environment.getProperty("local.server.port");
        currencyExchange.setEnvironment(port);
        return currencyExchange;
    }
}
