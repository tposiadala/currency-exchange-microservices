package com.tposiadala.microservices.currencyexchangeservice.controller;

import io.github.resilience4j.bulkhead.annotation.Bulkhead;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import io.github.resilience4j.ratelimiter.annotation.RateLimiter;
import io.github.resilience4j.retry.annotation.Retry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class CircuitBreakerController {

    private Logger logger = LoggerFactory.getLogger(CircuitBreakerController.class);

    @GetMapping("/sample-retry-api")
    @Retry(name = "sample-api", fallbackMethod = "hardcodedResponse")
    public String sampleRetryApi() {
        logger.info("Sample retry api call received");
		ResponseEntity<String> forEntity = new RestTemplate().getForEntity("http://localhost:8080/some-dummy-url", String.class);
		return forEntity.getBody();
    }

    @GetMapping("/sample-circuit-breaker-api")
    @CircuitBreaker(name = "default", fallbackMethod = "hardcodedResponse")
    public String sampleCircuitBreakerApi() {
        logger.info("Sample circuit breaker api call received");
        ResponseEntity<String> forEntity = new RestTemplate().getForEntity("http://localhost:8080/some-dummy-url", String.class);
        return forEntity.getBody();
    }

    @GetMapping("/sample-rate-limiter-api")
    @RateLimiter(name = "default")
    public String sampleRateLimiterApi() {
        logger.info("Sample rate limiter api call received");
        return "sample-rate-limiter-api";
    }

    @GetMapping("/sample-bulkhead-api")
    @Bulkhead(name = "sample-api")
    public String sampleApi() {
        logger.info("Sample bulkhead api call received");
        return "sample-bulkhead-api";
    }

    public String hardcodedResponse(Exception ex) {
        return "fallback-response";
    }
}
